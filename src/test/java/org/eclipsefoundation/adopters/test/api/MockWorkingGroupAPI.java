/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.adopters.test.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.core.service.APIMiddleware.BaseAPIParameters;
import org.eclipsefoundation.efservices.api.WorkingGroupsAPI;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.api.models.WorkingGroup.WorkingGroupParticipationAgreement;
import org.eclipsefoundation.efservices.api.models.WorkingGroup.WorkingGroupParticipationAgreements;
import org.eclipsefoundation.efservices.api.models.WorkingGroup.WorkingGroupParticipationLevel;
import org.eclipsefoundation.efservices.api.models.WorkingGroup.WorkingGroupResources;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;

@Mock
@RestClient
@ApplicationScoped
public class MockWorkingGroupAPI implements WorkingGroupsAPI {

    private List<WorkingGroup> wgs;

    public MockWorkingGroupAPI() {
        this.wgs = new ArrayList<>();
        this.wgs.addAll(Arrays.asList(WorkingGroup
                .builder()
                .setAlias("sample-wg")
                .setDescription("")
                .setLevels(Arrays.asList(WorkingGroupParticipationLevel.builder().setDescription("sample")
                        .setRelation("WGSAMP").build()))
                .setLogo("")
                .setParentOrganization("eclipse")
                .setResources(WorkingGroupResources
                        .builder()
                        .setCharter("")
                        .setContactForm("")
                        .setMembers("")
                        .setSponsorship("")
                        .setWebsite("")
                        .setParticipationAgreements(WorkingGroupParticipationAgreements
                                .builder()
                                .setIndividual(WorkingGroupParticipationAgreement
                                        .builder()
                                        .setDocumentId("sample-wg-iwgpa")
                                        .setPdf("sample-wg-iwgpa.pdf")
                                        .build())
                                .setOrganization(WorkingGroupParticipationAgreement
                                        .builder()
                                        .setDocumentId("sample-wg-owgpa")
                                        .setPdf("sample-wg-owgpa.pdf")
                                        .build())
                                .build())
                        .build())
                .setStatus("active")
                .setTitle("Sample WG")
                .build()));
    }

    @Override
    public Response get(BaseAPIParameters baseParams) {
        return Response.ok(wgs).build();
    }

    @Override
    public Response getAllByStatuses(BaseAPIParameters baseParams, List<String> statuses) {
        return Response.ok(wgs.stream().filter(wg -> statuses.contains(wg.getStatus())).collect(Collectors.toList()))
                .build();
    }
}
