/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.adopters.resources;

import java.util.List;
import java.util.Optional;

import jakarta.inject.Inject;

import org.eclipsefoundation.adopters.model.AdoptedProject;
import org.eclipsefoundation.adopters.test.helpers.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.response.ValidatableResponse;

@QuarkusTest
class AdoptersResourceTest {

    private static final String BASE_URL = "";
    private static final String HOW_TO_BE_LISTED = "/how-to-be-listed-as-an-adopter";
    private static final String WG_SUB_PAGE_URL = "/project-adopters/{wg}";
    private static final String ADOPTED_PROJECTS_URL = "/projects";
    private static final String ADOPTED_PROJECT_BY_WG_URL = ADOPTED_PROJECTS_URL + "?working_group={param}";
    private static final String SINGLE_PROJECT_URL = ADOPTED_PROJECTS_URL + "/{projectId}";

    private static final EndpointTestCase GET_ALL_SUCCESS = TestCaseHelper.buildSuccessCase(ADOPTED_PROJECTS_URL,
            new String[] {}, SchemaNamespaceHelper.ADOPTED_PROJECTS_SCHEMA_PATH);
    private static final EndpointTestCase GET_ALL_BY_WG_SUCCESS = TestCaseHelper.buildSuccessCase(
            ADOPTED_PROJECT_BY_WG_URL, new String[] { "sample-wg" },
            SchemaNamespaceHelper.ADOPTED_PROJECTS_SCHEMA_PATH);
    private static final EndpointTestCase GET_ALL_BY_PROJECT_SUCCESS = TestCaseHelper.buildSuccessCase(
            SINGLE_PROJECT_URL, new String[] { "sample.proj" }, SchemaNamespaceHelper.ADOPTED_PROJECTS_SCHEMA_PATH);

    @Inject
    ObjectMapper om;

    /*
     * GET /adopters
     */
    @Test
    void getBaseTemplatePage_success() {
        EndpointTestBuilder.from(EndpointTestCase.builder().setPath(BASE_URL).setStatusCode(200)
                .setResponseContentType(ContentType.HTML).build()).doGet().andCheckFormat().run();
    }

    /*
     * GET /adopters/how-to-be-listed-as-an-adopter
     */
    @Test
    void getHowToTemplatePage_success() {
        EndpointTestBuilder.from(EndpointTestCase.builder().setPath(HOW_TO_BE_LISTED).setStatusCode(200)
                .setResponseContentType(ContentType.HTML).build()).doGet()
                .run();
    }

    /*
     * GET /adopters/project-adopters/{wg}
     */
    @Test
    void getSubTemplatePage_success() {
        EndpointTestBuilder.from(EndpointTestCase.builder().setPath(WG_SUB_PAGE_URL).setStatusCode(200)
                .setResponseContentType(ContentType.HTML).setParams(Optional.of(new String[] { "sample-wg" })).build())
                .doGet().run();
    }

    @Test
    void getSubTemplatePage_success_invalidWG() {
        EndpointTestBuilder.from(EndpointTestCase.builder().setPath(WG_SUB_PAGE_URL).setStatusCode(200)
                .setResponseContentType(ContentType.HTML).setParams(Optional.of(new String[] { "invalid-wg" })).build())
                .doGet().run();
    }

    /*
     * GET /adopters/projects
     */
    @Test
    void getAdoptedProjects_success() {
        ValidatableResponse response = EndpointTestBuilder.from(GET_ALL_SUCCESS).doGet().run();
        List<AdoptedProject> adoptedProjects = parseJsonResponse(response.extract().body().asString());
        Assertions.assertTrue(!adoptedProjects.isEmpty(), "Results are expected");
    }

    @Test
    void getAdoptedProjects_success_validateSchema() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).doGet().andCheckSchema().run();
    }

    @Test
    void getAdoptedProjects_success_validateFormat() {
        EndpointTestBuilder.from(GET_ALL_SUCCESS).doGet().andCheckFormat().run();
    }

    /*
     * GET /adopters/projects?working_group={param}
     */
    @Test
    void getAdoptedProjectsByWG_success() {
        ValidatableResponse response = EndpointTestBuilder.from(GET_ALL_BY_WG_SUCCESS).doGet().run();
        List<AdoptedProject> adoptedProjects = parseJsonResponse(response.extract().body().asString());
        Assertions.assertTrue(!adoptedProjects.isEmpty(), "Results are expected");
    }

    @Test
    void getAdoptedProjectsByWG_success_validateSchema() {
        EndpointTestBuilder.from(GET_ALL_BY_WG_SUCCESS).doGet().andCheckSchema().run();
    }

    @Test
    void getAdoptedProjectsByWG_success_validateFormat() {
        EndpointTestBuilder.from(GET_ALL_BY_WG_SUCCESS).doGet().andCheckFormat().run();
    }

    @Test
    void getAdoptedProjects_success_invalidWG() {
        // Invalid working group ids return a 200 with an empty array
        ValidatableResponse response = EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(ADOPTED_PROJECT_BY_WG_URL, new String[] { "invalid-wg" },
                        SchemaNamespaceHelper.ADOPTED_PROJECTS_SCHEMA_PATH))
                .doGet().run();
        List<AdoptedProject> adoptedProjects = parseJsonResponse(response.extract().body().asString());
        Assertions.assertTrue(adoptedProjects.isEmpty(), "Results are not expected");
    }

    /*
     * GET /adopters/projects/{projectId}
     */
    @Test
    void getAdoptedProjectsByProject_success() {
        ValidatableResponse response = EndpointTestBuilder.from(GET_ALL_BY_PROJECT_SUCCESS).doGet().run();
        List<AdoptedProject> adoptedProjects = parseJsonResponse(response.extract().body().asString());
        Assertions.assertTrue(!adoptedProjects.isEmpty(), "Results are expected");
    }

    @Test
    void getAdoptedProjectsByProject_success_validateSchema() {
        EndpointTestBuilder.from(GET_ALL_BY_PROJECT_SUCCESS).doGet().andCheckSchema().run();
    }

    @Test
    void getAdoptedProjectsByProject_success_validateFormat() {
        EndpointTestBuilder.from(GET_ALL_BY_PROJECT_SUCCESS).doGet().andCheckFormat().run();
    }

    @Test
    void getAdoptedProjects_success_invalidProject() {
        // Invalid project ids return a 200 with an empty array
        ValidatableResponse response = EndpointTestBuilder
                .from(TestCaseHelper.buildSuccessCase(SINGLE_PROJECT_URL, new String[] { "invalid.proj" },
                        SchemaNamespaceHelper.ADOPTED_PROJECTS_SCHEMA_PATH))
                .doGet().run();
        List<AdoptedProject> adoptedProjects = parseJsonResponse(response.extract().body().asString());
        Assertions.assertTrue(adoptedProjects.isEmpty(), "Results are not expected");
    }

    private List<AdoptedProject> parseJsonResponse(String adoptedProjectBody) {
        try {
            return om.readerForListOf(AdoptedProject.class).readValue(adoptedProjectBody);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
