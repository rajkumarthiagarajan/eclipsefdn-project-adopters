/*********************************************************************
* Copyright (c) 2020, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.adopters.model;

import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.google.auto.value.AutoValue;

/**
 * A project with information about its adopters (read from the file system)
 * included with the object.
 * 
 * @author Martin Lowe
 *
 */
@AutoValue
@JsonDeserialize(builder = AutoValue_AdoptedProject.Builder.class)
public abstract class AdoptedProject {

	public abstract String getProjectId();

	public abstract String getName();

	public abstract String getUrl();

	public abstract String getLogo();

	public abstract List<Adopter> getAdopters();

	public static Builder builder() {
		return new AutoValue_AdoptedProject.Builder();
	}

	@AutoValue.Builder
	@JsonPOJOBuilder(withPrefix = "set")
	public abstract static class Builder {

		public abstract Builder setProjectId(String id);

		public abstract Builder setName(String name);

		public abstract Builder setUrl(String url);

		public abstract Builder setLogo(String logo);

		public abstract Builder setAdopters(List<Adopter> adopters);

		public abstract AdoptedProject build();
	}
}
