/*********************************************************************
* Copyright (c) 2020, 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Martin Lowe <martin.lowe@eclipse-foundation.org>
*		Zachary Sabourin <zachary.sabourin@ecliupse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.adopters.resource;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import org.eclipsefoundation.adopters.model.AdoptedProject;
import org.eclipsefoundation.adopters.model.Breadcrumb;
import org.eclipsefoundation.adopters.service.AdopterService;
import org.eclipsefoundation.core.service.CachingService;
import org.eclipsefoundation.efservices.api.models.Project;
import org.eclipsefoundation.efservices.api.models.WorkingGroup;
import org.eclipsefoundation.efservices.services.ProjectService;
import org.eclipsefoundation.efservices.services.WorkingGroupService;

import io.quarkus.qute.Location;
import io.quarkus.qute.Template;

/**
 * Retrieves adopted projects along with adopters info for display. This data can be viewed for all projects, a single
 * project, or all projects defined within a working group by the working group ID.
 * 
 * Additionally contains the HTML UI pages for users to interact with this data.
 * 
 * @author Martin Lowe
 *
 */
@Path("")
@Consumes({ MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON })
public class AdoptersResource {
    private static final String BREADCRUMB_TEMPLATE_PROPERTY = "breadcrumb";

    @Inject
    CachingService cache;

    @Inject
    ProjectService projectService;
    @Inject
    AdopterService adopterService;
    @Inject
    WorkingGroupService wgs;

    @Location("pages/project-adopters-wg")
    Template adoptersWgTemplate;
    @Location("pages/how-to-be-listed")
    Template howToBeListedTemplate;
    @Location("pages/home")
    Template homeTemplate;
    @Location("pages/error")
    Template errorTemplate;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getHomePage() {
        // Sort all Wgs alphabetically
        List<WorkingGroup> workingGroups = wgs.get();
        workingGroups.sort(Comparator.comparing(WorkingGroup::getTitle));
        return Response
                .ok(cache
                        .get("homepage-html", null, String.class,
                                () -> homeTemplate
                                        .data("workingGroups", workingGroups)
                                        .data(BREADCRUMB_TEMPLATE_PROPERTY, null)
                                        .render())
                        .getData()
                        .orElseGet(errorTemplate::render))
                .build();
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("how-to-be-listed-as-an-adopter")
    public Response getListedPage() {
        return Response
                .ok(cache
                        .get("how-to-be-listed", null, String.class,
                                () -> howToBeListedTemplate
                                        .data(BREADCRUMB_TEMPLATE_PROPERTY,
                                                new Breadcrumb("/adopters/how-to-be-listed-as-an-adopter",
                                                        "How to be listed as an Adopter"))
                                        .render())
                        .getData()
                        .orElseGet(errorTemplate::render))
                .build();
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    @Path("project-adopters/{alias}")
    public Response getWorkingGroupSubPage(@PathParam("alias") String workingGroupAlias) {
        // retrieve the corresponding working group, or return an error
        Optional<WorkingGroup> currentWg = wgs.getByName(workingGroupAlias);
        if (currentWg.isEmpty()) {
            // a bit redundant, but attempt to get a cached error page or render a fresh copy
            return Response
                    .ok(cache
                            .get(workingGroupAlias, null, String.class, errorTemplate::render)
                            .getData()
                            .orElseGet(errorTemplate::render))
                    .build();
        }
        // using the discovered working group, render the working group adopters page, w/ fallback on error page
        return Response
                .ok(cache
                        .get(workingGroupAlias, null, String.class,
                                () -> adoptersWgTemplate
                                        .data("wg", currentWg.get())
                                        .data(BREADCRUMB_TEMPLATE_PROPERTY,
                                                new Breadcrumb("/adopters/project-adopters/" + workingGroupAlias,
                                                        currentWg.get().getTitle()))
                                        .render())
                        .getData()
                        .orElseGet(errorTemplate::render))
                .build();
    }

    @GET
    @Path("/projects")
    public Response getAllAdopters(@QueryParam("working_group") String workingGroup) {
        // get cached project list
        List<Project> projects = projectService.getAllProjects();
        if (workingGroup != null && workingGroup.trim() != null) {
            projects = projects
                    .stream()
                    .filter(p -> p.getWorkingGroups().stream().anyMatch(wg -> wg.getId().equals(workingGroup)))
                    .collect(Collectors.toList());
        }
        // no projects for working group
        if (projects.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // get the adopted projects, removing non-adopted projects
        List<AdoptedProject> aps = adopterService.getAdoptedProjects(projects);
        if (aps == null) {
            return Response.serverError().build();
        }
        return Response.ok(aps).build();
    }

    @GET
    @Path("/projects/{projectId}")
    public Response getAdoptersForProject(@PathParam("projectId") String projectId) {
        // get cached project list
        List<Project> projects = projectService.getAllProjects();
        List<Project> filteredProjects = projects
                .stream()
                .filter(p -> p.getProjectId().equals(projectId))
                .collect(Collectors.toList());
        // no projects for working group
        if (filteredProjects.isEmpty()) {
            return Response.ok(Collections.emptyList()).build();
        }
        // get the adopted projects, removing non-adopted projects
        List<AdoptedProject> aps = adopterService.getAdoptedProjects(filteredProjects);
        if (aps == null) {
            return Response.serverError().build();
        }
        return Response.ok(aps).build();
    }
}
